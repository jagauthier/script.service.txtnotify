#!/usr/bin/env python

import threading
import time
import signal
import dbus
import dbus.service
import dbus.mainloop.glib
import gobject
import re
import subprocess

try:
    # so we can run this outside of kodi
    import xbmc
    import xbmcgui
    import xbmcaddon
    no_kodi=False
except ImportError:
    no_kodi=True
    pass

import os
import random

BUS_NAME='org.bluez.obex'
PATH = '/org/bluez/obex'
CLIENT_INTERFACE = 'org.bluez.obex.Client1'
SESSION_INTERFACE = 'org.bluez.obex.Session1'
MESSAGE_ACCESS_INTERFACE = 'org.bluez.obex.MessageAccess1'
MESSAGE_INTERFACE = 'org.bluez.obex.Message1'
TRANSFER_INTERFACE = 'org.bluez.obex.Transfer1'

# This is so we can run it without kodi

try:
    __addon__       = xbmcaddon.Addon(id='script.service.txtnotify')
    __addondir__    = xbmc.translatePath( __addon__.getAddonInfo('profile') )
except:
    pass

variables={}

def log(logline):
    print "TXTNOTIFY: " + logline

def set_kodi_prop(property, value):
#    log ("Setting: '{}', '{}'".format(property, value))
    strvalue=str(value)

    if no_kodi==True:
        variables[property]=value
    else:
        xbmcgui.Window(10000).setProperty(property, strvalue)

def get_kodi_prop(property):
    if no_kodi==True:
        if property in variables:
            value=variables[property]
        else:
            value=""
    else:
        value=xbmcgui.Window(10000).getProperty(property)
#    log("Loading: '" + property + "', '" + value + "'")
    return value

class TXTNotify(dbus.service.Object):
    msg_hdr_props=None
    msg_bdy_props=None
    
    def new_message(self, path, properties):
        
            # this isn't always called when it's a new message...
            # but for our purposes it's what we want.
            
            # Regex might be overkill, but I don't have every DBUS path
            # memorized or known. So I do know that this particular message
            # matches the below, so we're going to do it.
            
        result=re.match(r'.*message\d+', path)
        if result:
#            print
#            print properties
#            print
            self.msg_hdr_props=properties
            obj = self.bus.get_object(BUS_NAME, path)
            msg = dbus.Interface(obj, MESSAGE_INTERFACE)
#            msg.Get(__addondir__+"message.txt", True, reply_handler=self.create_transfer_reply,
#            	error_handler=self.error)
            msg.Get("", True, reply_handler=self.create_transfer_reply,
            	error_handler=self.error)
                
    def create_transfer_reply(self, path, properties):
		self.msg_bdy_props = properties
		log("Transfer created: %s (file %s)" % (path, self.msg_bdy_props["Filename"]))

    def error(self, err):
        log(err)

    def transfer_complete(self, path):
        if self.msg_bdy_props == None:
            return
        self.process_txt()

    def transfer_error(self, path):
		log("Transfer %s error" % path)

    def properties_changed(self, interface, properties, invalidated, path):
        # Regex might be overkill, but I want to make sure we only get client transfer messages
        
        result=re.match(r'.*session\d+/transfer\d+', path)
        if result:        
            if "Status" in properties:
                # print path        
                #  print properties
        
                if properties['Status'] == 'complete':
                    self.transfer_complete(path)
                    return

                if properties['Status'] == 'error':
                    self.transfer_error(path)
                    return

    def process_txt(self):
        log("File location: " + self.msg_bdy_props["Filename"])
        if os.path.exists(self.msg_bdy_props["Filename"]):
            log("File exists.")
        else:
            log("File does not exist.")
            
        # Just going to grab the stuffs
        # Just get BEGIN:MSG to END:MSG data.
        
        p = subprocess.Popen(['cat', self.msg_bdy_props["Filename"]], stdout=subprocess.PIPE, 
        	stderr=subprocess.PIPE)
        data=""
        fn=""
        n=""
        tel=""
        mfrom=""
        storeline=0

        for line in iter(p.stdout.readline,''):
            # the condition to store is in the middle so we don't get the header or footer
            if line[:7]=="END:MSG":
                storeline=0
                
            if storeline==1:
                data=data+line
                
            if line[:9]=="BEGIN:MSG":
                storeline=1

            if line[:3]=="FN:":
                fn=line[3:].strip()
            if line[:2]=="N:":
                n=line[2:].strip()
            if line[:4]=="TEL:":
                tel=line[4:].strip()
            
        if len(fn)>0:
            mfrom=fn
        else:
            if len(n)>0:
                mfrom=n
            else:
                mfrom=tel

        #remove the last two characters (strip doesn't work here.. why not?
        data=data[:len(data)-2]
        log("New Message: " + mfrom)
        log(" " + data)
        set_kodi_prop("txt_from", mfrom)
        set_kodi_prop("txt_data", data)
        
        muted=False
        
        if get_kodi_prop("muted_cnt")!="":
            for i in range(1,int(get_kodi_prop("muted_cnt"))+1):
                if mfrom == get_kodi_prop("muted_sender"+str(i)):
                    muted=True
                    log("User is muted.")
                    break
    
        if no_kodi==False and muted==False:
            log ("no_kodi: " + str(no_kodi))
            log ("muted:   " + str(muted))
            xbmc.executebuiltin('XBMC.RunScript("script.program.txtnotify")')
        
        # reset these because they are used for conditions on whether to process the transferred data or not
        self.msg_hdr_props=None
        self.msg_bdy_props=None
        
    def start(self):
        self.mainloop = gobject.MainLoop()
        self.mainloop.run()
        
    def end(self):
        if (self.mainloop):
            log("ENDING.")
            self.mainloop.quit()
        
    def __init__(self):

#        fprop={}
#        fprop["Filename"]="./obex-clientEMAIL"
#        self.msg_bdy_props=fprop
#        self.process_txt()
#        quit()
        
        device_mac=get_kodi_prop("connected_mac")
        log("Creating MAP session for " + device_mac + " (" + get_kodi_prop("connected_device") + ")")
        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
        self.bus = dbus.SystemBus()
        client = dbus.Interface(self.bus.get_object(BUS_NAME, PATH), CLIENT_INTERFACE)
        session_path = client.CreateSession(device_mac, { "Target": "MAP" })        
        obj = self.bus.get_object(BUS_NAME, session_path)
        self.session = dbus.Interface(obj, SESSION_INTERFACE)
        self.map = dbus.Interface(obj, MESSAGE_ACCESS_INTERFACE)

        self.bus.add_signal_receiver(self.new_message,
			dbus_interface="org.freedesktop.DBus.ObjectManager",
            signal_name="InterfacesAdded")

        self.bus.add_signal_receiver(self.properties_changed,
        	dbus_interface="org.freedesktop.DBus.Properties",
            signal_name="PropertiesChanged",
            path_keyword="path")

    def changeHandler():
        pass

def kodi_mon():

    monitor = xbmc.Monitor()
    while True:
        if monitor.waitForAbort(1):
            if 'txtnotify' in vars():
                if txtnotify:
                    TXTNotify.end(txtnotify)
            break

if __name__ == "__main__":

    gobject.threads_init()
    player = None
    log("Starting up")
    
    if no_kodi==False:
        threading.Thread(target=kodi_mon).start()

    # Technically, OBEX connections do not require an already existing connection.
    # But for our purposes, we are going to make it a requirement so the MAC address is 
    # easy to retrieve through the bluetooth poller interface.
    
    # So, if we are not connected.. we're going to wait here.
    
    if no_kodi==True:
        set_kodi_prop("connected_mac", "E4:FA:ED:44:F8:C9")
    
    while get_kodi_prop("connected_mac")=="" or get_kodi_prop("PBAP_download")=="1":
#        log("Waiting...")
        time.sleep(3)
        
    txtnotify = TXTNotify()
    txtnotify.start()
